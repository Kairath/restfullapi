import React, { Component } from "react";
import { Link, IndexLink } from "react-router";
import { connect } from "react-redux";



class Jumbotron extends Component {


	render() {
		
		let btnStyles = {
			background: "#56BFC5",
		} 
		
		return (
			<div className="jumbotron" id="home">
				<div className="col-md-6 col-md-offset-5 col-sm-6 col-sm-offset-6 col-xs-12">
					<form action="https://getsimpleform.com/messages?form_api_token=e1a48bc87441d724f42725fca93c823c" method="post"
						className="form-group col-md-6 col-sm-12 col-xs-12 col-md-offset-5">
					  <div className="formheader">
					  	<span ><i className="fa fa-envelope" aria-hidden="true" ></i></span>
					  	<span>Make an Appointment</span></div>
					  <fieldset className="form-group">
					  	<input className="form-control" type='text' name='name' placeholder="Name" />
					  </fieldset>
					  <fieldset className="form-group">
					  	<input className="form-control" type='phone' name='phone' placeholder="Your Phone"/>
					  </fieldset>
					  <fieldset className="form-group">
					  	<input className="form-control" type='email' name='email' placeholder="Your Email"/>
					  </fieldset>	
					  <fieldset className="form-group">
					  	<input className="form-control" type='text' name='message' placeholder="Your Message" />
					  </fieldset>
					  <fieldset className="form-group">
					  	<input className="btn btn-info" type='submit' value='Send' style={btnStyles} />
					  </fieldset>
					</form>
				</div>
					<div className="jumboStatic visible-lg visible-md">
						<h3>Dentist</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					</div>
			</div>
		)
	}
}

export default Jumbotron