import React, {Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Router, browserHistory } from 'react-router';

//containers
import Header from "./containers/Header";

//components
import Jumbotron from "./components/Jumbotron";
import SectionBand from "./components/SectionBand";
import About from "./components/About";
import Treatments from "./components/Treatments";
import Map from "./components/Map";
import Footer from "./Footer";

