import React, { Component } from 'react';
import {LocationSect, OpeningSect, ContactSect} from "./BandSection";


class Section extends Component {
	constructor(props){
		super(props)
	}
	
	
	render(){
		return (
			<div className="col-md-12" >
				<div className="row-eq-height">
					<ContactSect color={"#56BFC5"} icon="phone" header="Contact Us" />
					<OpeningSect color={"#40b4ba"} icon="clock-o" header="Opening Hours" />
					<LocationSect color={"#56BFC5"} icon="map-marker" header="Location" />
				</div>
			</div>
		)
	}
	
}

export default Section;