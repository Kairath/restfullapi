import React, { Component } from "react";


class Treatment extends Component {
    constructor(props){
        super(props)
        
    }
    
    render(){
        
        let styles = {
            textAlign: "center",
            marginBottom: "28px"
        }
        
        return(
                <div className="col-md-4 col-sm-12 treat" style={styles}>
                     <div style={{background: "#fff", padding: "20px"}}>
                        <i className={"fa fa-" + this.props.fa} aria-hidden="true"></i>
                        <h4> {this.props.header} </h4>
                        <p>  {this.props.text} </p>
                    </div>
                </div>
            )
    }
}

export default Treatment;