import React, { Component } from 'react';

//#3dadb2   #40b4ba   #44babf


class ContactSect extends Component {
	constructor(props){
		super(props)
	}
	
	render() {
		let color = this.props.color;
		let icon = this.props.icon;
		let divStyle = {
		  background: color,
		  padding: "40px",
		  textAlign: "center",
		  color: "white"
		};
		
		return (
			<div className="col-lg-4 col-md-4 col-sm-12 col-xs-12 bands" 
				style={divStyle} >
				<i className={"fa fa-" + icon } style={{  fontSize: "1.5em" }}></i>
				<h4>{this.props.header}</h4>
				<p>Phone: 932 50 540</p>
				<p>Emergency: 931 53 234</p>
				<p>Email: contact@yoursite.com</p>
			</div>
		)
	}
}


class OpeningSect extends Component {
	constructor(props){
		super(props)
	}
	
	render() {
		let color = this.props.color;
		let icon = this.props.icon;
		let divStyle = {
		  background: color,
		  padding: "40px",
		  textAlign: "center",
		  color: "white"
		};
		
		return (
			<div className="col-lg-4 col-md-4 col-sm-12 col-xs-12 bands" 
				style={divStyle} >
				<i className={"fa fa-" + icon } ></i>
				<h4>{this.props.header}</h4>
				<div className="col-md-5 col-sm-6 col-xl-6 bandhalf" style={{ textAlign: "left" }}>
					<p>Monday – Friday</p>
					<p>Saturday	</p>
					<p>Sunday</p>
				</div>
				<div className="col-md-6 col-sm-6 col-xl-6 bandhalf" style={{ textAlign: "right" }}>
					<p>9:00 – 18:00</p>
					<p>10:00 – 17:00</p>
					<p>10:00 – 15:00</p>
				</div>
			</div>
		)
	}
}

class LocationSect extends Component {
	constructor(props){
		super(props)
	}
	
	render() {
		let color = this.props.color;
		let icon = this.props.icon;
		let divStyle = {
		  background: color,
		  padding: "40px",
		  textAlign: "center",
		  color: "white"
		};
		
		return (
			<div className="col-lg-4 col-md-4 col-sm-12 col-xs-12 bands" 
				style={divStyle} >
				<i className={"fa fa-" + icon } style={{  fontSize: "1.5em" }}></i>
				<h4>{this.props.header}</h4>
				<p>Von schroeders 273, </p>
				<p>Viña del mar - Chile </p>
			</div>
		)
	}
}


export {LocationSect, OpeningSect, ContactSect};