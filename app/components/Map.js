import React, { Component } from "react";


export default class Map extends Component {
    constructor(props){
        super(props)
        
    }
    
    componentDidMount () {
            let myLatLng = { lat: 34.052234, lng: -118.243685 };

            let map = new google.maps.Map(document.querySelector('.map'), {
              zoom: 15,
              center: myLatLng
            });
            
            let marker = new google.maps.Marker({
              position: myLatLng,
              map: map,
              title: 'Dentist ...!'
            });
            
            let infowindow = new google.maps.InfoWindow({
                content: "Dentist X"
            });
            
            
            let opened = 0;
            marker.addListener('click', function() {
                if(opened == 0){
                    infowindow.open(map, marker);
                    opened = 1;
                } else {
                    infowindow.close();
                    opened = 0;
                }
            });
            
        }
        
 
    render(){
        
        let styles={
           height: "400px"
        }
        
        return(
            <div className="map col-md-12" id="location" style={styles}>
            </div>
        )
    }   
}