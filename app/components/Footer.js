import React, { Component } from "react";


class Footer extends Component {
    constructor(props){
        super(props)
        
    }
    
    render(){
        
        let styles = {
            listStyle: "none",
            display: "inline-block",
            color: "black"
        };
        
        let iconLinks = {
            facebook: "www.google.com",
            twitter: "www.google.com",
            instagram: "www.google.com",
            youtube: "www.google.com",
            skype: "nolink"
        }
        
        let linksRenderification = () => {
            let links = [];
            let index = 0;
            
            for(var key in iconLinks ){
                if(key != "nolink"){
                  index++;
                }
                
            };
            
            return links;
        }
        
        
        return(
                <footer className="col-md-12 col-sm-12" 
                    style={{background:"#40b4ba", padding: "60px 0 20px 0", textAlign: "center", color:"white" }} >
                    <div>
                        <ul className="icons " style={styles}>
                            <li><a href="#"><i className="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i className="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i className="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i className="fa fa-youtube" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i className="fa fa-skype" aria-hidden="true"></i></a></li>
                        </ul>
                        <p><small>&copy; 2016 Dentist - All rights reserved </small></p>
                    </div>
                        
                </footer>
            )
    }
}

export default Footer;