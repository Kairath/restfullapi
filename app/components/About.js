import React, { Component } from "react";


class About extends Component {
    
    
    render (){
        
        let aboutUs = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis nostrum ab vero temporibus magnam, delectus!";
        let about1 = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam explicabo unde doloremque quo, fugit, nisi.";
        let about2 = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam explicabo unde doloremque quo, fugit, nisi.";
        let about3 = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam explicabo unde doloremque quo, fugit, nisi.";
        
        
        let iconStyle = { border: "2px solid #40b4ba", padding: "5px", color:"#40b4ba"};
        let divStyle = { padding: "10px" }
        let explaStyle = { paddingLeft: "10px", fontSize: "1em" }
        
        return(
                <div className="container">
                    <div className="" id="about" style={{ paddingBottom: "14px" }}>
                        <div className="col-md-12 col-sm-12 aboutheader" style={{ textAlign: "center", padding: "20px 0" }}><h3>About Us</h3></div>
                        <div className="row">
                            <div className="col-md-6">
                                <p>{aboutUs}</p>
                                <div style={divStyle}>
                                    <i className="fa fa-heart" style={iconStyle}></i><span style={explaStyle}>{about1}</span>
                                </div>
                                <div style={divStyle}>
                                    <i className="fa fa-medkit" style={iconStyle}></i><span style={explaStyle}>{about2}</span>
                                </div>
                                <div style={divStyle}>
                                    <i className="fa fa-user-md" style={iconStyle}></i><span style={explaStyle}>{about3}</span>
                                </div>
                            </div>
                            <div className="col-md-6 about">
                                <img src="./img/doctor-clinic.jpg" alt="about us clinic image"/>
                            </div>
                        </div>
                    </div>
                </div>
            )
    }
}

export default About;