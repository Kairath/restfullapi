import React, { Component } from "react";
import Treatment from "./Treatment";


class Treatments extends Component {
    
    
    render(){
        
        let treats = [
            [
                "Invisible braces", 
                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam minima, itaque molestiae ut ratione unde?"
            ],
            [
                "Cosmetic Dentistry", 
                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam minima, itaque molestiae ut ratione unde?"
            ],    
            [
                "Stain removal", 
                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam minima, itaque molestiae ut ratione unde?"
            ],    
            [
                "Dentures", 
                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam minima, itaque molestiae ut ratione unde?"
            ],    
            [
                "Emergency Calls", 
                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam minima, itaque molestiae ut ratione unde?"
            ],    
            [
                "Sedation", 
                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam minima, itaque molestiae ut ratione unde?"
            ]
        ];
        
        let icons = [
                "user-md", "diamond", "flask", "globe", "ambulance", "bed"
            ]
        
        let treatRender = () => {
            let restult = [];
            for(let i = 0; i < treats.length; i++){
                restult.push( <Treatment key={i} header={treats[i][0]} text={treats[i][1]} fa={icons[i]}/> )
            }
            
            return restult;
        }
        
        return(
                <div className="col-md-12" style={{ background: "#d3d3d3", padding: "30px 0" }}>
                    <div className="container" id="treatments">
                        <div className="col-md-12 col-sm-12" style={{ textAlign: "center", paddingBottom: "10px", marginBottom: "10px"}}>
                            <h3>Treatments</h3>
                        </div>
                        { treatRender() }
                    </div>
                </div>
            )
    }
}

export default Treatments;