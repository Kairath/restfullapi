import React, { Component } from "react";
import { connect } from "react-redux";


class Navbar extends Component {

	render() {
		
		let styles = {
			
		}
	
				
		return (
			<div style={styles}>
				<nav className="navbar navbar-default navbar-fixed-top" id="navbarheader">
				  <div className="col-md-10 col-md-offset-1">
				  	<div className="" >
					    <div className="navbar-header">
					      <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
					        <span className="sr-only">Toggle navigation</span>
					        <span className="icon-bar"></span>
					        <span className="icon-bar"></span>
					        <span className="icon-bar"></span>
					      </button>
					      <a className="navbar-brand" style={{color: "#40b4ba"}} href="#">Dentist</a>
					    </div>

					    <div className="collapse navbar-collapse" id="navbar-collapse-1">
					      <ul className="nav navbar-nav navbar-right">
					        <li className="active"><a href="#home"> Home <span className="sr-only">(current)</span></a></li>
					        <li><a href="#about">About</a></li>
					        <li><a href="#treatments">Treatments</a></li>
				            <li><a href="#location">Location</a></li>
					      </ul>
					    </div>
					</div>
				  </div>
				</nav>
			</div>
		)
	}
}

export default Navbar