function $http(url) {
   
   var core = {
       ajax: function(method, url, args){
            var promise = new Promise(function(resolve, reject){
                var http = new XMLHttpRequest();
                var uri = url;
                
                if(args && ( method == "POST" || method == "PUT")){
                    uri += "?";
                    var argcount = 0;
                    for(var key in args){
                        if(Object.prototype.hasOwnProperty.call(args, key)){
                            if (argcount++) {
                                uri += '&';
                            }
                            uri += encodeURIComponent(key) + "=" + encodeURIComponent(args(key));
                        }
                    }
                }
                
                http.open(method, uri);
                http.send();
                
                http.onload = function () {
                    if(this.status >= 200 && this.status < 300){
                        resolve(this.response);
                    } else {
                        reject(this.statusText);
                    }
                };
            });   
            
            return promise;
       }
   };
   
   return {
       "get": function (args) {
           return core.ajax('GET', url, args);
       },
       'post' : function(args) {
          return core.ajax('POST', url, args);
        },
        'put' : function(args) {
          return core.ajax('PUT', url, args);
        },
        'delete' : function(args) {
          return core.ajax('DELETE', url, args);
        }
   };
   
}

onmessage = function(e){ //e.data  postMessage
  
};