var gulp = require("gulp");
var sass = require("gulp-sass");
var browserSync = require("browser-sync").create();
var uglify = require("gulp-uglify");
var sourcemaps = require("gulp-sourcemaps");
var source = require("vinyl-source-stream");
var buffer = require("vinyl-buffer");
var browserify = require("browserify");
var babel = require("babelify");
var useref = require("gulp-useref");
var gulpIf = require("gulp-if");
var cssnano = require("gulp-cssnano");
var imagemin = require("gulp-imagemin");
var cache = require("gulp-cache");
var runSequence = require("run-sequence");
var mocha = require("gulp-mocha");
var autoprefixer = require("gulp-autoprefixer");
var rename = require("gulp-rename");
var gutil = require("gulp-util");
var del = require("del");

gulp.task("default", ["watch"], function(){
  console.log("initialization started");
});

gulp.task("mocha", function () {
  return gulp.src(["test/*.js"], { read: false})
    .pipe(mocha({reporter: "list"}))
    .on("error", gutil.log);
});

gulp.task("sass", function(){
    return gulp.src("./assets/scss/**/*.scss")
           .pipe(sass().on('error', sass.logError))
           .pipe(autoprefixer({ 
              browsers: ['last 2 versions'] }))
           .pipe(gulp.dest("./public/css"))
           .pipe(browserSync.reload({
              stream: true
            }));
});

gulp.task("watch",["browserSync", "sass"], function() {
    gulp.watch("./assets/scss/**/*.scss", ["sass"]);
    gulp.watch('./public/*.html', browserSync.reload); 
    gulp.watch('./assets/js/**/*.js', browserSync.reload);
    gulp.watch("./app/**/*.js", ["scripts"]);
    gulp.watch("./assets/js/**/*.js", ["scripts"]);
    gulp.watch(["./app/**/*.js", "./test/**/*.js","./public/js/**/*.js"], ["mocha"]);
});

gulp.task("browserSync", function(){
  browserSync.init({
    server: {
      baseDir: "./public"
    }
  })
});
//

gulp.task('scripts', function(){
  var bundler = browserify({
    entries: ['./app/app.js'], // main js file and files you wish to bundle
    debug: true,
    extensions: ['js']
  }).transform(babel.configure({
    presets: ["es2015", "react"] //sets the preset to transpile to es2015 (you can also just define a .babelrc instead)
  }));

  // bundler is simply browserify with all presets set
  bundler.bundle()
    .on('error', function(err) { console.error(err); this.emit('end'); })
    .pipe(source('app.js')) // main source file
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true })) // create sourcemap before running edit commands so we know which file to reference
      .pipe(uglify()) //minify file
      .pipe(rename("main-min.js")) // rename file
    .pipe(sourcemaps.write('./', {sourceRoot: './js'})) // sourcemap gets written and references wherever sourceRoot is specified to be
    .pipe(gulp.dest('./public/js/'))
    .pipe(browserSync.reload({
      stream: true
   }));
});


//build tasks//
gulp.task("useref", function(){
  return gulp.src("./assets/**/*")
       .pipe(useref())
       //.pipe(gulpIf("*.js", uglify())) //gives error include js files in app.js
       .pipe(gulpIf("*.css", cssnano()))
       .pipe(gulp.dest("public"))
});

gulp.task("images", function(){
  return gulp.src("./assets/img/**/*.+(png|jpg|gif|svg)")
       .pipe(cache(imagemin({
          interlaced: true,
          progressive: true,
          multipass: true,
          optipng: 3
       })))
       .pipe(gulp.dest("./public/img"))
});

gulp.task("fonts", function(){
  return gulp.src("./assets/fonts/**/*")
       .pipe(gulp.dest("./public/fonts"))
});


gulp.task("del", function(){
  return del.sync("./public/css","./public/img","./ublic/js")
})

gulp.task("build", function(callback){
  runSequence("del",
    ["sass","scripts", "mocha", "useref", "images", "fonts"],
    callback
  )
});